import re, string
import nltk
import numpy as np


class Preprocess:
    KATA_DASAR = []
    DATA_KBBI = []

    def __init__(self):
        global KATA_DASAR
        global DATA_KBBI
        KATA_DASAR = [
            line.strip('\n') for line in open('dictionary/rootword.txt')
        ]
        DATA_KBBI = [
            kamus.strip('\n').strip('\r') for kamus in open(
                'dictionary/kbba.txt'
            )
        ]

    def tokenize(self, text):
        token = nltk.word_tokenize(text)
        return token

    def kbbi(self, token):
        global DATA_KBBI

        dic = dict()
        for i in DATA_KBBI:
            (key, val) = i.split('\t')
            dic[str[key]] = val

        final_string = ' '.join(
            str(dic.get(word, word)) for word in token
        ).split()

        return final_string

    def normalize_token(self, _tokens):
        tokens = self.kbbi(_tokens)
        return tokens

    def preprocess(self, text):

        def hapus_tanda(text):
            tanda_baca = set(string.punctuation)
            tweet = ''.join(ch for ch in text if ch not in tanda_baca)
            return text

        def hapus_kata_double(s):
            pattern = re.compile(r"(.)\1{1,}", re.DOTALL)
            return pattern.sub(r"\1\1", s)

        text = text.lower()
        text = re.sub(r'\\u\w\w\w\w', '', text)
        text = re.sub(r'https\S+', '', text)
        text = re.sub(r"@([^|s]+)", "", text)
        text = re.sub(r"#([^\s]+)", r"\1", text)
        text = hapus_tanda(text)
        text = re.sub(r'\w*\d\w*', '', text).strip()
        text = hapus_kata_double(text)
        return text

    def prep(self, sent):
        return self.normalize_token(self.tokenize(self.preprocess(sent)))

