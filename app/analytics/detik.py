__author__ = "Nasa"

import requests
from bs4 import BeautifulSoup
import urllib.parse
import re

from app.analytics.analyze import process_sentiment

COMMENT_URL = 'https://newcomment.detik.com/graphql?query={ search(type: "comment",size: 10 ,page:1,sort:"newest", query: [{name: "news.artikel", terms: "article_id" } , {name: "news.site", terms: "dtk"} ]) { paging  counter }  }'

def search_detik(keyword):
    params = urllib.parse.urlencode({"query": keyword})
    res = requests.get("https://www.detik.com/search/searchall?" + params)
    soup = BeautifulSoup(res.content, features="html.parser")
    articles = soup.find_all('article')
    result_data = []
    for article in articles:
        title = article.find('h2', class_="title").get_text()
        content = article.find('p')
        if content is not None:
            content = content.get_text()
        else:
            content = ""
        date = article.find('span', class_="date")
        if date is not None:
            date = date.get_text().split(", ")[1]
        else:
            date = ""
        link = article.find('a')['href']
        # print(link)
        if "e-flash" in link or "detikflash" in link:
            continue
        article_raw = requests.get(link)
        article_soup = BeautifulSoup(article_raw.content, features="html.parser")
        detail_text_raw = article_soup.find('div', 'detail_text').get_text().replace("\n", " ").replace("\t", "")
        clean_detail_text = detail_text_raw.replace("\n", " ").replace("\t", "")
        text = re.sub(r"(<!--//.*-->)", "", clean_detail_text).strip()
        article_id = link.split("d-")[1].split("/")
        if article_id is not None:
            # print(article_id)
            article_id = article_id[0]
        url = COMMENT_URL.replace("article_id", article_id)
        # print(url)
        comments = requests.get(url)
        comments = comments.json()

        # print(comments)

        if comments is not None:
            # print(comments)
            data = comments.get("data", {})
            if data:
                search = data.get("search", {})
                if search:
                    hits = search.get("hits", {})
                    if hits:
                        comment_results = hits.get("results", [])
                    else:
                        comment_results = []
                else:
                    comment_results = []
            else:
                comment_results = []
            comment_count = comments.get("data", {}).get("search", {}).get("counter", 0)
        else:
            comment_results = []
            comment_count = 0

        result_data.append(
            {
                "title": title,
                "date": date,
                "link": link,
                "text": text,
                "snippet": content,
                "comments": {
                    "comments": comment_results,
                    "comment_count": comment_count
                },
                "sentiment": process_sentiment(text)
            }
        )

    return result_data

if __name__ == "__main__":
    print(search_detik("Pemilu"))
