from flask import url_for, request, jsonify
from app import db
from app.analytics import bp
from app.analytics.twitter import search as twitter_search, twitter_overview, twitter_accounts
from app.analytics.facebook import search_feed
from app.analytics.kompas import search_kompas
from app.analytics.detik import search_detik


@bp.route('')
def analytics():
    return 'analytics main page'


@bp.route('/tw')
def twitter():
    results = twitter_search(status="Ford")
    return str(results)


@bp.route('/tw/search', methods=["POST"])
def search_twitter():
    search_q = request.get_json(force=True).get("q", "")
    results = twitter_search(search_q)
    return jsonify(results)


@bp.route('/fb/search', methods=["POST"])
def search_facebook():
    keyword = request.get_json(force=True).get("q", "")
    results = search_feed(keyword)
    return jsonify(results)

@bp.route('/ks/search', methods=["POST"])
def kompas_search():
    keyword = request.get_json(force=True).get("q", "")
    results = search_kompas(keyword)
    return jsonify(results)


@bp.route('/tw/overview', methods=["POST"])
def get_twitter_overview():
    twitter_id = request.get_json(force=True).get("twitter_id", "")
    results = twitter_overview(twitter_id)
    return jsonify(results)

@bp.route('/tw/accounts', methods=['GET'])
def get_twitter_accounts():
    result = twitter_accounts()
    return jsonify(result)

@bp.route('/dtk/search', methods=['POST'])
def detik_search():
    keyword = request.get_json(force=True).get("q", "")
    results = search_detik(keyword)
    return jsonify(results)

