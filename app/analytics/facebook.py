from app import fb_user_graph_api, fb_page_graph_api
from app.analytics.analyze import process_sentiment


def get_user_friend_list():
    friends = fb_user_graph_api.get_connections("me", "friends")
    return friends


def get_feeds():
    feeds = fb_user_graph_api.get_connections(
        'me', 'feed', fields='message,comments{message,reactions},reactions,created_time'
    )
    return feeds


def process_feed(feed):
    data = dict()
    data["message"] = feed['message']
    data['sentiment'] = process_sentiment(feed['message'])
    data['shares'] = feed.get("shares", 0)
    reactions = feed.get("reactions")
    data['reactions'] = {"like": 0, "love": 0, "haha": 0, "wow": 0, "sad": 0, "angry": 0, "total": 0}
    if reactions is not None:
        total_reactions = 0
        for reaction in reactions['data']:
            if str(reaction['type']).lower() not in data['reactions']:
                data['reactions'].update({str(reaction['type']).lower(): 1})
            else:
                data['reactions'][str(reaction['type']).lower()] += 1
            total_reactions += 1
        data['reactions']['total'] = total_reactions
    data['interactions'] = data['reactions']['total'] + data['shares']
    comments = feed.get("comments", 0)
    if comments is not None:
        if "paging" in comments:
            comments.pop('paging')
    if comments != 0:
        comments = len(comments)
    data['comments'] = comments
    return data


def search_feed(keyword):
    feeds = get_feeds()
    filtered_feeds = []
    for feed in feeds['data']:
        message = feed.get("message")
        if message is None:
            continue
        if message.find(keyword) >= 0:
            feed = process_feed(feed)
            filtered_feeds.append(feed)
    return filtered_feeds

    
