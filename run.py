from app import create_app, db, twitter_api, fb_user_graph_api, fb_page_graph_api
# from app.models import User


app = create_app()

@app.shell_context_processor
def make_shell_context():
    return {
        'db': db,
        'user_graph_api': fb_user_graph_api,
        "page_graph_api": fb_page_graph_api,
        'twitter_api':twitter_api,
#        'preprocess_text': preproses_text
    }

