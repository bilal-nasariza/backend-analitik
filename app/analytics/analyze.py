from app.analytics.preprocess import Preprocess
from textblob import TextBlob

preprocess = Preprocess()


def process_sentiment(status_text):
    status = TextBlob(preprocess.preprocess(status_text))
    if status.sentiment[0] > 0:
        sentiment = "Positive"
    elif status.sentiment[0] < 0:
        sentiment = "Negative"
    else:
        sentiment= "Neutral"
    return sentiment
