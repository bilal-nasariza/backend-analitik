__author__ = "Nasa"

import os

from app import google_service, db
from app.analytics.analyze import process_sentiment
import requests
from bs4 import BeautifulSoup


def search_kompas(keyword):
    # res = google_service.cse().list(q=keyword, cx=os.environ.get("KOMPAS_COM_CS", ""), num=100).execute()
    cx=os.environ.get("KOMPAS_COM_CS", "")
    url = "https://www.googleapis.com/customsearch/v1?q="
    google_api_key = os.environ.get("GOOGLE_API_KEY", "")
    res = requests.get(url + keyword + '&cx=' + cx + '&key=' + google_api_key + '&alt=json')
    res = res.json()
    items = res.get("items")
    search_result = []
    for item in items:
        feed = requests.get(item.get("link"))
        soup = BeautifulSoup(feed.content, features="html.parser")
        news_content = soup.find('div', class_="read__content").get_text().strip()
        sentiment = process_sentiment(news_content)
        meta_tags = item.get("pagemap").get("metatags", [])[0]
        comments_url = os.environ.get("KOMPAS_APIS_URL", "http://unknown")
        comments_url += "/".join(item.get("link").split("/")[4:8])
        comments = requests.get(comments_url)
        comments_soup = BeautifulSoup(comments.content, features="html.parser")
        comment_list = comments_soup.find_all("div", class_="comment-text")
        comment_data = []
        for c in comment_list:
            c_tag = c.find_all('span')
            if len(c_tag) >= 4:
                c_author = c_tag[1].get_text()
                c_content = c_tag[3].get_text()
                c_sentiment = process_sentiment(c_content)
            else:
                c_author = ""
                c_content = ""
                c_sentiment = ""
            
            if c_author == '' and c_content == "":
                continue
            comment_data.append(
                {
                    "author": c_author,
                    "comment": c_content,
                    "sentiment": c_sentiment
                }
            )
        search_result.append(
            {
                "title": item.get("title", ""),
                "snippet": item.get("snippet", ''),
                "content_author": meta_tags.get("content_author"),
                "editor": meta_tags.get("content_editor"),
                "tag": meta_tags.get("content_tag"),
                "sentiment": sentiment,
                "comments": comment_data,
                "comment_count": len(comment_data)
            }
        )
    return search_result


if __name__ == "__main__":
    print(search_kompas("pemilu"))

