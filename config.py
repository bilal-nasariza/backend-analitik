import os
from dotenv import load_dotenv


base_dir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(base_dir, '.flaskenv'))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'super-secret-key'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
            'sqlite:///' + os.path.join(base_dir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TWEEPY_CONSUMER_KEY = os.environ.get("TWITTER_CONSUMER_KEY") or ""
    TWEEPY_CONSUMER_SECRET = os.environ.get("TWITTER_CONSUMER_KEY_SECRET") or ""
    TWEEPY_ACCESS_TOKEN_KEY = os.environ.get("TWITTER_ACCESS_TOKEN") or ""
    TWEEPY_ACCESS_TOKEN_SECRET = os.environ.get("TWITTER_ACCESS_TOKEN_SECRET")\
             or ""
    FACEBOOK_ACCESS_TOKEN = "608928762896127|ymWDnfO9wLdPB8cOQNmZHYn7bKA"



