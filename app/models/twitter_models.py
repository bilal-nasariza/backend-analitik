from collections import OrderedDict
from datetime import datetime, timedelta
from app import db


class TwitterAccount(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), index=True, unique=True)
    screen_name = db.Column(db.String(120), index=True)
    followers = db.relationship(
        'FollowerOverview',
        backref='account',
        lazy='dynamic'
    )
    contents = db.relationship(
        'ContentOverview',
        backref='author',
        lazy='dynamic'
    )
    engagements = db.relationship(
        'EngagementOverview',
        backref='engaged_account',
        lazy='dynamic'
    )

    def __repr__(self):
        return '<TwitterAccount {}>'.format(self.id)



class FollowerOverview(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    twitter_id = db.Column(db.Integer, db.ForeignKey('twitter_account.id'))
    total_followers = db.Column(db.Integer, default=0)
    total_following = db.Column(db.Integer, default=0)
    total_listed = db.Column(db.Integer, default=0)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<FollowerOverview {} for {}>'.format(self.id, self.twitter_id)


class ContentOverview(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    twitter_id = db.Column(db.Integer, db.ForeignKey('twitter_account.id'))
    tweet_id = db.Column(db.Integer, index=True)
    tweet_text = db.Column(db.String, default="")
    profile_tweets = db.Column(db.Integer, default=0)
    profile_retweets = db.Column(db.Integer, default=0)
    profile_replies = db.Column(db.Integer, default=0)
    total_retweets = db.Column(db.Integer, default=0)
    total_likes = db.Column(db.Integer, default=0)
    total_replies = db.Column(db.Integer, default=0)
    mention = db.Column(db.Integer, default=0)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<ContentOverview {} for {}>'.format(self.id, self.twitter_id)


class EngagementOverview(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    twitter_id = db.Column(db.Integer, db.ForeignKey('twitter_account.id'))
    tweet_id = db.Column(db.Integer, index=True)
    total_likes = db.Column(db.Integer, default=0)
    total_replies = db.Column(db.Integer, default=0)
    total_retweets = db.Column(db.Integer, default=0)
    mention = db.Column(db.Integer, index=True)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<EngagementOverview {} for {}>'.format(self.id, self.twitter_id)
