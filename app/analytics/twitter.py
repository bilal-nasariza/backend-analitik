import urllib
from datetime import datetime, timedelta
from sqlalchemy import and_, func
from app import twitter_api, db
from app.analytics.analyze import process_sentiment
from app.models.twitter_models import (
    FollowerOverview,
    ContentOverview, 
    EngagementOverview,
    TwitterAccount
)


def search(status, tweet_mode="extended"):
    search_results = twitter_api.search(
        urllib.parse.quote(status),
        tweet_mode=tweet_mode,
        count=100,
        result_type="mixed"
    )
    statuses = []
    for result in search_results:
        sentiment = process_sentiment(result.full_text)
        tweet_status = result._json
        tweet_status['sentiment'] = sentiment
        statuses.append(tweet_status)
    return statuses

def twitter_accounts():
    accounts = TwitterAccount.query.all()
    ret = [
        {
            'id': account.id,
            'name': account.name,
            'screen_name': account.screen_name,
            'total_followers': account.followers[-1].total_followers,
            'sum_of_profile_tweets': account.contents[-1].profile_tweets
        }
        for account in accounts
    ]
    return ret


def twitter_overview(twitter_id):
    since = datetime.now() - timedelta(days=7)
    follower_overview = FollowerOverview.query.filter(
        and_(
            FollowerOverview.twitter_id==twitter_id,
            FollowerOverview.timestamp > since
        )
    )
    content_overview= get_aggregated_content_overview(twitter_id)
    engagement_overview = EngagementOverview.query.filter_by(
        twitter_id=twitter_id
    )

    follower_overview = [
        {
            "total_followers": fo.total_followers,
            "total_following": fo.total_following,
            "total_listed": fo.total_listed,
            "id": fo.id,
            "date": fo.timestamp.strftime("%Y-%m-%d")
        } for fo in follower_overview
    ]
    content_overview = [
        {
            "profile_tweets": co.profile_tweets,
            "profile_retweets": co.profile_retweets,
            "profile_replies": co.profile_replies,
            "total_likes": co.total_likes,
            "total_replies": co.total_replies,
            "total_retweets": co.total_retweets,
            "mention": co.mention,
            "text": co.tweet_text,
            "date": co.timestamp.strftime("%Y-%m-%d")
        } for co in content_overview
    ]

    engagement_overview = [
        {
            "total_likes": eo.total_likes,
            "total_replies": eo.total_replies,
            "total_retweets": eo.total_retweets,
            "mention": eo.mention,
            "date": eo.timestamp.strftime("%Y-%m-%d"),
        } for eo in engagement_overview
    ]

    overview = {
        "follower": follower_overview,
        "content": content_overview,
        "engagement": []
    }
    return overview


def update_follower_overview(twitter_id):
    current_acct = TwitterAccount.query.get(twitter_id)
    twitter_profile = twitter_api.get_user(twitter_id)

    fo_exists = FollowerOverview.query.filter(and_(
        FollowerOverview.twitter_id == twitter_id,
        func.DATE(FollowerOverview.timestamp) == datetime.now().date()
    )).first()

    if fo_exists:
        fo_exists.total_followers=twitter_profile.followers_count
        fo_exists.total_listed = twitter_profile.listed_count
    else:
        fo = FollowerOverview(
            total_followers=twitter_profile.followers_count,
            total_listed = twitter_profile.listed_count,
            account=current_acct
        )
        db.session.add(fo)
    db.session.commit()
    return {"account": current_acct.name, "id": current_acct.id}


def update_content_overview(twitter_id):
    time_lines = twitter_api.user_timeline(twitter_id)
    twitter_profile = TwitterAccount.query.get(twitter_id)
    for status in time_lines:
        print(status.user.statuses_count)
        content_exists = ContentOverview.query.filter(
            ContentOverview.tweet_id==status.id
        ).first()

        if content_exists is not None:
            content_exists.total_retweets = status.retweet_count
            content_exists.total_likes=status.favorite_count
            content_exists.profile_tweets=status.user.statuses_count
        else:

            co = ContentOverview(
                twitter_id=twitter_id,
                total_replies=0,
                tweet_id=status.id,
                total_likes=status.favorite_count,
                total_retweets=status.retweet_count,
                mention=0,
                tweet_text=status.text,
                timestamp=status.created_at,
                profile_tweets=status.user.statuses_count
            )
            db.session.add(co)
        db.session.commit()
    return {"total": len(time_lines)}


def update_daily_twitter_data():
    accounts = TwitterAccount.query.all()
    for account in accounts:
        update_follower_overview(account.id)
        update_content_overview(account.id)
    return {"updated_account": [{"id": act.id} for act in accounts]}

def get_aggregated_content_overview(twitter_id):
    since = datetime.now() - timedelta(days=7)
    q = db.session.query(
        func.count(ContentOverview.tweet_id).label("profile_tweets"),
        func.sum(ContentOverview.total_retweets).label("total_retweets"),
        func.sum(ContentOverview.total_replies).label("total_replies"),
        func.sum(ContentOverview.total_likes).label("total_likes"),
        func.sum(ContentOverview.profile_replies).label("profile_replies"),
        func.sum(ContentOverview.profile_retweets).label("profile_retweets"),
        func.sum(ContentOverview.mention).label("mention"),
        ContentOverview.tweet_text,
        ContentOverview.timestamp
    ).group_by(
        func.DATE(ContentOverview.timestamp)
    ).filter(and_(
        ContentOverview.twitter_id==twitter_id
        )
    )
    return q[-10:]