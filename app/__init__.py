import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from config import Config
import tweepy
from facebook import GraphAPI
from googleapiclient.discovery import build
from app.utilities.json_helper import AlchemyEncoder

# from app.analytics.preprocess import Preprocess

db = SQLAlchemy()
migrate = Migrate()

twitter_auth = tweepy.OAuthHandler(
    os.environ.get("TWITTER_CONSUMER_KEY", ""),
    os.environ.get("TWITTER_CONSUMER_KEY_SECRET", "")
)

twitter_auth.set_access_token(
    os.environ.get("TWITTER_ACCESS_TOKEN", ""),
    os.environ.get("TWITTER_ACCESS_TOKEN_SECRET", "")
)

twitter_api = tweepy.API(twitter_auth)

# preprocess_text = Preprocess()

google_service = build("customsearch", "v1", developerKey=os.environ.get("GOOGLE_API_KEY", ""))

fb_user_graph_api = GraphAPI(access_token=os.environ.get("FACEBOOK_USER_ACCESS_TOKEN", ""))
fb_page_graph_api = GraphAPI(
    access_token=os.environ.get("FACEBOOK_PAGE_ACCESS_TOKEN", "")
)

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)
    app.json_encoder = AlchemyEncoder

    db.init_app(app)
    migrate.init_app(app, db)
    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    from app.analytics import bp as analytics_bp
    app.register_blueprint(analytics_bp, url_prefix='/analytics')
    cors = CORS(app, resources={r"/*": {"origins": "*"}})

    return app

from app.models import twitter_models


